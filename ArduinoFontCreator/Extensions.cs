﻿//-----------------------------------------------------------------------
// <copyright file="Extensions.cs" company="Castores Inc.">
//     Custom company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FontCreator
{
    /// <summary>
    /// Extensions class
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Creates the byte array.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <param name="value">The value.</param>
        /// <returns>Creates a byte array populated</returns>
        public static byte[] CreateByteArray(int length, byte value)
        {
            byte[] arr = new byte[length];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = value;
            }

            return arr;
        }
    }
}
