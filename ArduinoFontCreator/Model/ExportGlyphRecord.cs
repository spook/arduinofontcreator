﻿//-----------------------------------------------------------------------
// <copyright file="ExportGlyphRecord.cs" company="Castores Inc.">
//     Custom company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FontCreator
{
    /// <summary>
    /// ExportGlyphRecord class
    /// </summary>
    public class ExportGlyphRecord
    {
        /// <summary>
        /// Gets or sets the bitmap offset.
        /// </summary>
        public int BitmapOffset { get; set; }

        /// <summary>
        /// Gets or sets the glyph.
        /// </summary>
        public Glyph Glyph { get; set; }
    }
}
