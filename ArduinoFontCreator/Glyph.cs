﻿//-----------------------------------------------------------------------
// <copyright file="Glyph.cs" company="Castores Inc.">
//     Custom company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace FontCreator
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Newtonsoft.Json;

    /// <summary>
    /// Glyph class
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public class Glyph : INotifyPropertyChanged
    {
        /// <summary>
        /// The image
        /// </summary>
        private BitmapSource image;

        /// <summary>
        /// The bitmap data
        /// </summary>
        private byte[] bitmapData;

        /// <summary>
        /// The width
        /// </summary>
        private int width;

        /// <summary>
        /// The height
        /// </summary>
        private int height;

        /// <summary>
        /// The advance x
        /// </summary>
        private int advanceX;

        /// <summary>
        /// The offset x
        /// </summary>
        private int offsetX;

        /// <summary>
        /// The offset y
        /// </summary>
        private int offsetY;

        /// <summary>
        /// The modified
        /// </summary>
        private bool modified;

        /// <summary>
        /// The used
        /// </summary>
        private bool used;

        /// <summary>
        /// Initializes a new instance of the <see cref="Glyph"/> class.
        /// </summary>
        /// <param name="manager">The manager.</param>
        public Glyph(GlyphsManager manager)
        {
            Manager = manager;
            CanvasPixels = new ObservableCollection<Rectangle>();
            Width = 5;
            Height = 5;
            Modified = false;
            AutoGenerateBorder = true;
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the canvas pixels.
        /// </summary>
        [JsonProperty("pixels")]
        [JsonConverter(typeof(CanvasPixelsConverter))]
        public ObservableCollection<Rectangle> CanvasPixels { get; set; }

        /// <summary>
        /// Gets or sets the ASCII.
        /// </summary>
        [JsonProperty("ascii")]
        public byte Ascii { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the manager.
        /// </summary>
        [JsonIgnore]
        public GlyphsManager Manager { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Glyph"/> is used.
        /// </summary>
        [JsonProperty("used")]
        public bool Used
        {
            get => used;
            set
            {
                if (used == value)
                {
                    return;
                }

                used = value;
                OnPropertyChanged("Used");
                Modified = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Glyph"/> is modified.
        /// </summary>
        [JsonProperty("modified")]
        public bool Modified
        {
            get => modified;
            set
            {
                if (modified == value)
                {
                    return;
                }

                modified = value;
                OnPropertyChanged("Modified");
                if (Manager != null)
                {
                    Manager.Modified = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the bitmap data.
        /// </summary>
        [JsonIgnore]
        public byte[] BitmapData
        {
            get => bitmapData;
            set
            {
                bitmapData = value;
                OnPropertyChanged("BitmapData");
            }
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        [JsonProperty("width")]
        public int Width
        {
            get => width;
            set
            {
                if (width == value)
                {
                    return;
                }

                width = value;
                UpdatePreview();
                OnPropertyChanged("Width");
                Modified = true;
            }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        [JsonProperty("height")]
        public int Height
        {
            get => height;
            set
            {
                if (height == value)
                {
                    return;
                }

                height = value;
                UpdatePreview();
                OnPropertyChanged("Height");
                Modified = true;
            }
        }

        /// <summary>
        /// Gets or sets the offset x.
        /// </summary>
        [JsonProperty("advanceX")]
        public int AdvanceX
        {
            get => advanceX;
            set
            {
                if (advanceX == value)
                {
                    return;
                }

                advanceX = value;
                UpdatePreview();
                OnPropertyChanged("AdvanceX");
                Modified = true;
            }
        }

        /// <summary>
        /// Gets or sets the offset x.
        /// </summary>
        [JsonProperty("offsetX")]
        public int OffsetX
        {
            get => offsetX;
            set
            {
                if (offsetX == value)
                {
                    return;
                }

                offsetX = value;
                UpdatePreview();
                OnPropertyChanged("OffsetX");
                Modified = true;
            }
        }

        /// <summary>
        /// Gets or sets the offset y.
        /// </summary>
        [JsonProperty("offsetY")]
        public int OffsetY
        {
            get => offsetY;
            set
            {
                if (offsetY == value)
                {
                    return;
                }

                offsetY = value;
                UpdatePreview();
                OnPropertyChanged("OffsetY");
                Modified = true;
            }
        }

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        [JsonIgnore]
        public BitmapSource Image
        {
            get => image;
            set
            {
                image = value;
                OnPropertyChanged("Image");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [automatic generate border].
        /// </summary>
        [JsonIgnore]
        public bool AutoGenerateBorder { get; set; }

        /// <summary>
        /// Sets the pixel.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="pixelType">Type of the pixel.</param>
        /// <param name="auto">if set to <c>true</c> [automatic].</param>
        public void SetPixel(int x, int y, PixelType pixelType, bool auto = false)
        {
            if (x < 0 || x >= 50 || y < 0 || y >= 50)
            {
                return;
            }

            Rectangle rect = CanvasPixels.FirstOrDefault(_ =>
            {
                double left = Canvas.GetLeft(_);
                double top = Canvas.GetTop(_);
                return left.Equals(x * 20) && top.Equals(y * 20);
            });

            if (rect == null)
            {
                CreatePixel(x, y, pixelType);
                if (pixelType == PixelType.Foreground && AutoGenerateBorder && auto == false)
                {
                    CreateBorder(x, y);
                }

                UpdatePreview();
            }
            else
            {
                ////if (auto && (rect.Fill as SolidColorBrush).Color == Colors.Red && pixelType == PixelType.Background)
                ////{
                ////    CanvasPixels.Remove(rect);
                ////}

                if (auto == false)
                {
                    CanvasPixels.Remove(rect);

                    SolidColorBrush brush = rect.Fill as SolidColorBrush;
                    Debug.Assert(brush != null, "Brush is null");

                    if (brush.Color == Colors.Red && pixelType == PixelType.Background ||
                        brush.Color == Colors.Green && pixelType == PixelType.Foreground)
                    {
                        CreatePixel(x, y, pixelType);
                        if (pixelType == PixelType.Foreground && AutoGenerateBorder)
                        {
                            CreateBorder(x, y);
                        }
                    }
                    else
                    {
                        if (brush.Color == Colors.Red && AutoGenerateBorder && pixelType == PixelType.Foreground)
                        {
                            RemoveBorder(x, y);
                            if (IsTaken(x - 1, y - 1) ||
                                IsTaken(x + 0, y - 1) ||
                                IsTaken(x + 1, y - 1) ||
                                IsTaken(x - 1, y + 0) ||
                                IsTaken(x + 1, y + 0) ||
                                IsTaken(x - 1, y + 1) ||
                                IsTaken(x + 0, y + 1) ||
                                IsTaken(x + 1, y + 1))
                            {
                                SetPixel(x, y, PixelType.Background, true);
                            }
                        }
                    }

                    UpdatePreview();
                    Modified = true;
                }
            }
        }

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="name">The name.</param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Creates the border.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        private void CreateBorder(int x, int y)
        {
            ////SetPixel(x - 1, y - 1, PixelType.Background, true);
            SetPixel(x + 0, y - 1, PixelType.Background, true);
            ////SetPixel(x + 1, y - 1, PixelType.Background, true);
            SetPixel(x - 1, y + 0, PixelType.Background, true);
            SetPixel(x + 1, y + 0, PixelType.Background, true);
            ////SetPixel(x - 1, y + 1, PixelType.Background, true);
            SetPixel(x + 0, y + 1, PixelType.Background, true);
            ////SetPixel(x + 1, y + 1, PixelType.Background, true);
        }

        /// <summary>
        /// Removes the border.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        private void RemoveBorder(int x, int y)
        {
            RemoveBackgroundBorder(x - 1, y - 1);
            RemoveBackgroundBorder(x + 0, y - 1);
            RemoveBackgroundBorder(x + 1, y - 1);
            RemoveBackgroundBorder(x - 1, y + 0);
            RemoveBackgroundBorder(x + 1, y + 0);
            RemoveBackgroundBorder(x - 1, y + 1);
            RemoveBackgroundBorder(x + 0, y + 1);
            RemoveBackgroundBorder(x + 1, y + 1);
        }

        /// <summary>
        /// Removes the background border.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        private void RemoveBackgroundBorder(int x, int y)
        {
            Rectangle rect = CanvasPixels.FirstOrDefault(_ =>
            {
                double left = Canvas.GetLeft(_);
                double top = Canvas.GetTop(_);
                return left.Equals(x * 20) && top.Equals(y * 20);
            });

            if (rect == null)
            {
                return;
            }

            SolidColorBrush brush = rect.Fill as SolidColorBrush;
            Debug.Assert(brush != null, "Brush is null");

            if (brush.Color != Colors.Green)
            {
                return;
            }

            ////if (IsTaken(x - 1, y - 1) == false &&
            ////    IsTaken(x + 0, y - 1) == false &&
            ////    IsTaken(x + 1, y - 1) == false &&
            ////    IsTaken(x - 1, y + 0) == false &&
            ////    IsTaken(x + 1, y + 0) == false &&
            ////    IsTaken(x - 1, y + 1) == false &&
            ////    IsTaken(x + 0, y + 1) == false &&
            ////    IsTaken(x + 1, y + 1) == false)
            if (IsTaken(x + 0, y - 1) == false &&
                IsTaken(x - 1, y + 0) == false &&
                IsTaken(x + 1, y + 0) == false &&
                IsTaken(x + 0, y + 1) == false)
            {
                CanvasPixels.Remove(rect);
            }
        }

        /// <summary>
        /// Determines whether the specified x is taken.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns>
        ///   <c>true</c> if the specified x is taken; otherwise, <c>false</c>.
        /// </returns>
        private bool IsTaken(int x, int y)
        {
            Rectangle rect = CanvasPixels.FirstOrDefault(_ =>
            {
                double left = Canvas.GetLeft(_);
                double top = Canvas.GetTop(_);
                return left.Equals(x * 20) && top.Equals(y * 20);
            });

            if (rect == null)
            {
                return false;
            }

            SolidColorBrush brush = rect.Fill as SolidColorBrush;
            Debug.Assert(brush != null, "Brush is null");

            return brush.Color == Colors.Red;
        }

        /// <summary>
        /// Updates the preview.
        /// </summary>
        private void UpdatePreview()
        {
            if (Width == 0 || Height == 0)
            {
                Image = null;
                return;
            }

            WriteableBitmap writeableBitmap = new WriteableBitmap(Width, Height, 96, 96, PixelFormats.Rgb24, null);
            byte[] renderData = Extensions.CreateByteArray(writeableBitmap.BackBufferStride * Height, 255);

            foreach (Rectangle rect in CanvasPixels)
            {
                int x = ((int)Canvas.GetLeft(rect)) / 20;
                int y = ((int)Canvas.GetTop(rect)) / 20;

                if (x >= Width || y >= Height)
                {
                    continue;
                }

                int posY = y * writeableBitmap.BackBufferStride;

                int r = x * 3 + posY;
                int g = r + 1;
                int b = r + 2;

                PixelType pixelType = PixelType.None;

                SolidColorBrush brush = rect.Fill as SolidColorBrush;
                Debug.Assert(brush != null, "Brush is null");

                if (brush.Color == Colors.Red)
                {
                    pixelType = PixelType.Foreground;
                }
                else if (brush.Color == Colors.Green)
                {
                    pixelType = PixelType.Background;
                }

                switch (pixelType)
                {
                    case PixelType.Foreground:
                    {
                        renderData[r] = 0;
                        renderData[g] = 0;
                        renderData[b] = 0;
                        break;
                    }

                    case PixelType.Background:
                    {
                        renderData[r] = 0;
                        renderData[g] = 255;
                        renderData[b] = 0;
                        break;
                    }
                }
            }

            writeableBitmap.WritePixels(new System.Windows.Int32Rect(0, 0, Width, Height), renderData, writeableBitmap.BackBufferStride, 0);
            Image = writeableBitmap;
        }

        /// <summary>
        /// Creates the pixel.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="pixelType">Type of the pixel.</param>
        private void CreatePixel(int x, int y, PixelType pixelType)
        {
            Rectangle rect = new Rectangle
            {
                Width = 22,
                Height = 22,
                StrokeThickness = 2,
                Stroke = new SolidColorBrush(Colors.Black),
                Fill = new SolidColorBrush(pixelType == PixelType.Foreground ? Colors.Red : Colors.Green)
            };
            Canvas.SetLeft(rect, x * 20);
            Canvas.SetTop(rect, y * 20);

            CanvasPixels.Add(rect);
            Modified = true;
        }
    }
}
