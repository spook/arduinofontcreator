﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace FontCreator.Exports
{
    public class ExportIli9341_t3 : IExport
    {
        private class BitTools
        {
            public static byte UnsignedBitsNeededFor(uint maxValue)
            {
                byte result = 0;
                while (maxValue > 0)
                {
                    result++;
                    maxValue >>= 1;
                }

                return Math.Max((byte)1, result);
            }

            public static byte SignedBitsNeededFor(int minValue, int maxValue)
            {
                byte maxValueBits, minValueBits;
                if (maxValue >= 0)
                    maxValueBits = (byte)(UnsignedBitsNeededFor((uint)maxValue) + 1);
                else
                    maxValueBits = (byte)(UnsignedBitsNeededFor((uint)(-maxValue - 1)) + 1);

                if (minValue >= 0)
                    minValueBits = (byte)(UnsignedBitsNeededFor((uint)minValue) + 1);
                else
                    minValueBits = (byte)(UnsignedBitsNeededFor((uint)(-minValue - 1)) + 1);

                return Math.Max(minValueBits, maxValueBits);
            }
        }

        private class GlyphStats
        {           
            public GlyphStats(uint maxWidth, 
                uint maxHeight, 
                int minXOffset,
                int maxXOffset,
                int minYOffset,
                int maxYOffset, 
                uint maxDelta)
            {
                WidthBits = BitTools.UnsignedBitsNeededFor((uint)maxWidth);
                HeightBits = BitTools.UnsignedBitsNeededFor((uint)maxHeight);
                XOffsetBits = BitTools.SignedBitsNeededFor(minXOffset, maxXOffset);
                YOffsetBits = BitTools.SignedBitsNeededFor(minYOffset, maxYOffset);
                DeltaBits = BitTools.UnsignedBitsNeededFor((uint)maxDelta);
            }

            public byte WidthBits { get; }
            public byte HeightBits { get; }
            public byte XOffsetBits { get; }
            public byte YOffsetBits { get; }
            public byte DeltaBits { get; }
        }

        private class CharRange
        {
            public CharRange(byte codeFrom, byte codeTo)
            {
                IndexFrom = codeFrom;
                IndexTo = codeTo;
            }

            public byte IndexFrom { get; }
            public byte IndexTo { get; }
        }

        private IEnumerable<int> CombineRanges(CharRange range1, CharRange range2)
        {
            if (range1 != null)
                for (int i = range1.IndexFrom; i <= range1.IndexTo; i++)
                    yield return i;

            if (range2 != null)
                for (int i = range2.IndexFrom; i <= range2.IndexTo; i++)
                    yield return i;
        }

        private (CharRange, CharRange) EvaluateCharRanges(List<Glyph> glyphs)
        {
            // Find biggest gap between used chars

            int? gapStart = null, gapEnd = null;
            int? biggestGapStart = null, biggestGapEnd = null;
            byte? firstChar = null, lastChar = null;

            for (int i = 0; i < glyphs.Count; i++)
            {
                if (glyphs[i] != null && glyphs[i].Used)
                {
                    if (firstChar == null)
                        firstChar = (byte)i;
                    lastChar = (byte)i;

                    if (gapStart != null)
                    {
                        // Make sure, that gapEnd points before used character
                        gapEnd = i - 1;

                        if (biggestGapStart == null || biggestGapEnd == null || gapEnd - gapStart > biggestGapEnd - biggestGapStart)
                        {
                            biggestGapStart = gapStart;
                            biggestGapEnd = gapEnd;
                        }

                        gapStart = null;
                        gapEnd = null;
                    }
                }
                else
                {
                    if (gapStart == null && firstChar != null)
                    {
                        // Make sure, that gapStart points after used character was found
                        gapStart = i;
                    }
                }
            }

            if (firstChar == null || lastChar == null)
            {
                // No characters to process
                return (null, null);
            }
            else if (biggestGapStart == null)
            {
                return (new CharRange(firstChar.Value, lastChar.Value), null);
            }
            else
            {
                return (new CharRange(firstChar.Value, (byte)(biggestGapStart - 1)), new CharRange((byte)(biggestGapEnd + 1), lastChar.Value));
            }
        }

        private GlyphStats EvaluateGlyphStats(List<Glyph> glyphs)
        {
            uint maxWidth = 0;
            uint maxHeight = 0;
            int minXOffset = 0;
            int maxXOffset = 0;
            int minYOffset = 0;
            int maxYOffset = 0;
            uint maxDelta = 0;
                
            for (int i = 0; i < glyphs.Count; i++)
            {
                if (glyphs[i] != null && glyphs[i].Used)
                {
                    maxWidth = (uint)Math.Max(maxWidth, glyphs[i].Width);
                    maxHeight = (uint)Math.Max(maxHeight, glyphs[i].Height);
                    minXOffset = Math.Min(minXOffset, glyphs[i].OffsetX);
                    maxXOffset = Math.Max(maxXOffset, glyphs[i].OffsetX);
                    minYOffset = Math.Min(minYOffset, glyphs[i].OffsetY);
                    maxYOffset = Math.Max(maxYOffset, glyphs[i].OffsetY);
                    maxDelta = (uint)Math.Max(maxDelta, glyphs[i].AdvanceX);
                }
            }

            return new GlyphStats(maxWidth, maxHeight, minXOffset, maxXOffset, minYOffset, maxYOffset, maxDelta);
        }

        private bool CheckSameRow(byte[,] rows, int y1, int y2)
        {
            for (int x = 0; x < rows.GetLength(0); x++)
                if (rows[x, y1] != rows[x, y2])
                    return false;

            return true;
        }

        private void BuildByteData(StringBuilder sb, List<byte> data)
        {
            sb.Append("  ");

            int bytesInRow = 0;
            bool first = true;
            for (int i = 0; i < data.Count; i++)
            {
                if (bytesInRow == 16)
                {
                    sb.AppendLine("");
                    sb.Append("  ");
                    bytesInRow = 0;
                }

                sb.Append($"0x{data[i].ToString("X2")}");
                if (i < data.Count - 1)
                    sb.Append(", ");

                bytesInRow++;
            }

            if (bytesInRow < 16)
                sb.AppendLine("");
        }

        private (StringBuilder, StringBuilder) BuildFileContents(string filename, 
            List<Glyph> glyphs,
            List<byte> bitmapData, 
            List<byte> indexData, 
            CharRange range1, 
            CharRange range2,
            byte bitsIndex,
            byte bitsWidth,
            byte bitsHeight,
            byte bitsXOffset,
            byte bitsYOffset,
            byte bitsDelta,
            byte lineSpace,
            byte capHeight)
        {
            System.Diagnostics.Debug.WriteLine($"Range1: {range1?.IndexFrom ?? 0}-{range1?.IndexTo ?? 0}");
            System.Diagnostics.Debug.WriteLine($"Range2: {range2?.IndexFrom ?? 0}-{range2?.IndexTo ?? 0}");
            System.Diagnostics.Debug.WriteLine($"BitsIndex: {bitsIndex}");
            System.Diagnostics.Debug.WriteLine($"BitsWidth: {bitsWidth}");
            System.Diagnostics.Debug.WriteLine($"BitsHeight: {bitsHeight}");
            System.Diagnostics.Debug.WriteLine($"BitsXOffset: {bitsXOffset}");
            System.Diagnostics.Debug.WriteLine($"BitsYOffset: {bitsYOffset}");
            System.Diagnostics.Debug.WriteLine($"BitsDelta: {bitsDelta}");
            System.Diagnostics.Debug.WriteLine($"LineSpace: {lineSpace}");
            System.Diagnostics.Debug.WriteLine($"CapHeight: {capHeight}");

            StringBuilder cFile = new StringBuilder();
            StringBuilder hFile = new StringBuilder();

            string fontName = Path.GetFileNameWithoutExtension(filename);

            // H file

            string includeGuard = "__" + Path.GetFileName(Path.ChangeExtension(filename, "h")).Replace('.', '_') + "__";
            hFile.AppendLine($"#ifndef {includeGuard}");
            hFile.AppendLine($"#define {includeGuard}");

            hFile.AppendLine("");
            hFile.AppendLine("#include <ILI9341_t3.h>");
            hFile.AppendLine("");
            hFile.AppendLine("#ifdef __cplusplus");
            hFile.AppendLine("extern \"C\" {");
            hFile.AppendLine("#endif");
            hFile.AppendLine("");
            hFile.AppendLine($"extern const ILI9341_t3_font_t {fontName};");
            hFile.AppendLine("");
            hFile.AppendLine("#ifdef __cplusplus");
            hFile.AppendLine("} // extern \"C\"");
            hFile.AppendLine("#endif");
            hFile.AppendLine("");
            hFile.AppendLine("#endif");

            // C file

            cFile.AppendLine($"#include \"{Path.ChangeExtension(Path.GetFileName(filename), "h")}\"");
            cFile.AppendLine("");
            cFile.AppendLine($"static const unsigned char {fontName}_data[] = {{");

            BuildByteData(cFile, bitmapData);

            cFile.AppendLine($"}};");
            cFile.AppendLine($"/* Size: {bitmapData.Count} bytes */");
            cFile.AppendLine("");
            cFile.AppendLine($"static const unsigned char {fontName}_index[] = {{");

            BuildByteData(cFile, indexData);

            cFile.AppendLine("};");
            cFile.AppendLine($"/* Size: {indexData.Count} bytes */");
            cFile.AppendLine("");
            cFile.AppendLine($"const ILI9341_t3_font_t {fontName} = {{");
            cFile.AppendLine($"    {fontName}_index,");
            cFile.AppendLine($"    0,");
            cFile.AppendLine($"    {fontName}_data,");
            cFile.AppendLine($"    1,");
            cFile.AppendLine($"    0,");

            Action<CharRange> appendRange = (CharRange range) =>
            {
                if (range != null)
                {
                    cFile.AppendLine($"    {glyphs[range.IndexFrom].Ascii},");
                    cFile.AppendLine($"    {glyphs[range.IndexTo].Ascii},");
                }
                else
                {
                    cFile.AppendLine($"    0,");
                    cFile.AppendLine($"    0,");
                }
            };

            appendRange(range1);
            appendRange(range2);

            cFile.AppendLine($"    {bitsIndex},");
            cFile.AppendLine($"    {bitsWidth},");
            cFile.AppendLine($"    {bitsHeight},");
            cFile.AppendLine($"    {bitsXOffset},");
            cFile.AppendLine($"    {bitsYOffset},");
            cFile.AppendLine($"    {bitsDelta},");
            cFile.AppendLine($"    {lineSpace},");
            cFile.AppendLine($"    {capHeight}");
            cFile.AppendLine("};");

            return (hFile, cFile);
        }


        public bool Export(string fileName, IList<Glyph> rawGlyphs, bool legacy)
        {
            List<Glyph> glyphs = Enumerable.Range(0, 256)
                .Select(code => new { Code = code, Glyph = rawGlyphs.FirstOrDefault(g => g.Ascii == code) })
                .Select(pair => pair.Glyph)
                .ToList();

            StringBuilder sb = new StringBuilder();

            (CharRange range1, CharRange range2) = EvaluateCharRanges(glyphs);
            if (range1 == null || range2 == null)
                return false;

            uint[] bitIndices = new uint[glyphs.Count];
            for (int i = 0; i < bitIndices.Length; i++)
                bitIndices[i] = 0;

            // *** Bit statistics ***
            GlyphStats stats = EvaluateGlyphStats(glyphs);

            // *** Prepare bitmap data ***
            BitBuffer buffer = new BitBuffer();

            foreach (var index in CombineRanges(range1, range2))
            {
                Console.WriteLine($"--- Char {glyphs[index].Ascii} ({(char)(glyphs[index].Ascii)}) index {buffer.CurrentBit} ---");

                // Store current index
                if (buffer.CurrentBit % 8 != 0)
                    throw new InvalidOperationException("Data buffer is not padded to byte boundary!");

                bitIndices[index] = buffer.CurrentBit / 8;

                // Reserved: 3 bits
                buffer.PushUnsignedInt(0b000, 3);
                System.Diagnostics.Debug.WriteLine(" (reserved)");

                // Glyph header
                buffer.PushUnsignedInt((uint)glyphs[index].Width, stats.WidthBits);
                System.Diagnostics.Debug.WriteLine(" (Width)");
                buffer.PushUnsignedInt((uint)glyphs[index].Height, stats.HeightBits);
                System.Diagnostics.Debug.WriteLine(" (Height)");
                buffer.PushSignedInt(glyphs[index].OffsetX, stats.XOffsetBits);
                System.Diagnostics.Debug.WriteLine(" (XOffset)");
                buffer.PushSignedInt(glyphs[index].OffsetY, stats.YOffsetBits);
                System.Diagnostics.Debug.WriteLine(" (YOffset)");
                buffer.PushUnsignedInt((uint)glyphs[index].AdvanceX, stats.DeltaBits);
                System.Diagnostics.Debug.WriteLine(" (Delta)");

                // Prepare rows
                byte[,] rows = new byte[glyphs[index].Width, glyphs[index].Height];
                for (int x = 0; x < rows.GetLength(0); x++)
                    for (int y = 0; y < rows.GetLength(1); y++)
                        rows[x, y] = 0;

                foreach (var pixel in glyphs[index].CanvasPixels)
                {
                    int x = (int)Canvas.GetLeft(pixel) / 20;
                    int y = (int)Canvas.GetTop(pixel) / 20;

                    if (x >= 0 && y >= 0 && x < rows.GetLength(0) && y < rows.GetLength(1))
                        if (pixel.Fill is SolidColorBrush solidFill)
                            if (solidFill.Color == Colors.Red)
                                rows[x, y] = 1;
                }

                // Encode rows
                int row = 0;
                while (row < rows.GetLength(1))
                {
                    int repeatedRow = row;
                    while (repeatedRow < rows.GetLength(1) - 2 && CheckSameRow(rows, row, repeatedRow + 1) && repeatedRow - row + 1 < 9)
                        repeatedRow++;

                    if (repeatedRow == row)
                    {
                        buffer.PushBit(0);
                        System.Diagnostics.Debug.Write("|");

                        for (int x = 0; x < rows.GetLength(0); x++)
                            buffer.PushBit(rows[x, row]);
                        System.Diagnostics.Debug.WriteLine(" (Regular row)");

                        row++;
                    }
                    else
                    {
                        int count = (repeatedRow - row + 1);

                        buffer.PushBit(1);
                        System.Diagnostics.Debug.Write("|");

                        buffer.PushUnsignedInt((uint)(count - 2), 3);
                        System.Diagnostics.Debug.Write("|");

                        for (int x = 0; x < rows.GetLength(0); x++)
                            buffer.PushBit(rows[x, row]);

                        System.Diagnostics.Debug.WriteLine(" (Repeated row)");

                        row = repeatedRow + 1;
                    }
                    
                }

                // Pad to byte boundary
                buffer.PadToByte();
                System.Diagnostics.Debug.WriteLine(" (Pad to byte)");
            }

            List<byte> bitmapData = buffer.Pack();
            System.Diagnostics.Debug.WriteLine(" (Pad to byte)");

            // *** Prepare index data ***

            // Evaluate required bit count

            // Yeah, can be done more efficiently, I'm lazy
            uint maxIndex = 0;
            for (int i = 0; i < bitIndices.Length; i++)
                maxIndex = (uint)Math.Max(maxIndex, bitIndices[i]);

            byte indexBitCount = BitTools.UnsignedBitsNeededFor(maxIndex);

            foreach (var index in CombineRanges(range1, range2))
            {
                buffer.PushUnsignedInt(bitIndices[index], indexBitCount);
                System.Diagnostics.Debug.WriteLine(" (index)");
            }

            List<byte> indexData = buffer.Pack();
            System.Diagnostics.Debug.WriteLine(" (Pad to byte)");

            // Generate file

            // TODO evaluate line height and cap size

            (StringBuilder hFile, StringBuilder cFile) = BuildFileContents(fileName, 
                glyphs, 
                bitmapData, 
                indexData,
                range1,
                range2,
                indexBitCount,
                stats.WidthBits,
                stats.HeightBits,
                stats.XOffsetBits,
                stats.YOffsetBits,
                stats.DeltaBits,
                0,
                10);  // TODO

            string hFilename = Path.ChangeExtension(fileName, "h");
            File.WriteAllText(hFilename, hFile.ToString());
            string cFilename = Path.ChangeExtension(fileName, "cpp");
            File.WriteAllText(cFilename, cFile.ToString());

            return true;
        }
    }
}
